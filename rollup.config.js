import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import flow from "rollup-plugin-flow";

export default {
	input: "src/index.js",
	output: {
		format: "umd",
		name: "reduxu-chain",
		file: "dist/reduxu-chain.js",
	},	
	plugins: [
		flow({ pretty: true }),
		resolve({
			jsnext: true,
			main: true,
			browser: true,
		}),
		commonjs(),
	],
};
