/* @flow */

// import { random, range } from "lodash";
import React, { Component } from "react";
import { ScrollView, StyleSheet } from "react-native";

import MyCard from "./MyCard";
import Victory from "./Victory";

// import {
// 	Title,
// 	Caption,
// 	Paragraph,
// 	Card,
// 	CardContent,
// } from "react-native-paper";

export default class MyApp extends Component {
	render() {
		return (
			<ScrollView contentContainerStyle={styles.container} scrollEnabled>
				<MyCard>
					<Victory />
				</MyCard>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: "center",
		backgroundColor: "#e1d7cd",
		justifyContent: "center",
	},
});
