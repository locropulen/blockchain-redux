import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

class MyApp extends Component<{
	theme: ThemeType,
}> {
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.description}>
						This demo shows how to instance a custom immutable blockchain and
						mine yourself! care your mobile battery man, you are really minning
						:)
					</Text>
				</View>
				{this.props.children}
			</View>
		);
	}
}

export default MyApp;

const styles = StyleSheet.create({
	container: {
		backgroundColor: "white",
		flex: 1,
	},
	bottom: {
		alignItems: "flex-end",
		backgroundColor: "transparent",
		paddingHorizontal: 10,
	},
	header: {
		paddingTop: 40,
		paddingBottom: 20,
		paddingHorizontal: 12,
		backgroundColor: "#336699",
	},
	description: {
		fontSize: 14,
		color: "white",
		paddingVertical: 10,
	},
	title: {
		paddingHorizontal: 12,
		fontSize: 14,
		paddingVertical: 9,
		backgroundColor: "#999",
		color: "white",
	},
});
