/* @flow */

import { random, range } from "lodash";
import React, { Component } from "react";
import { View, StyleSheet, Platform, Text } from "react-native";

import {
	VictoryChart,
	VictoryGroup,
	VictoryStack,
	VictoryBar,
	VictoryLine,
	VictoryArea,
} from "victory-native";

const styles = StyleSheet.create({
	text: {
		fontSize: 18,
		fontFamily: Platform.OS === "ios" ? "Menlo" : "monospace",
		fontWeight: "bold",
		marginTop: 25,
		marginBottom: 20,
	},
});

export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			scrollEnabled: true,
			y: this.getYFunction(),
			style: this.getStyles(),
			transitionData: this.getTransitionData(),
			randomData: this.generateRandomData(),
			staticRandomData: this.generateRandomData(15),
			data: this.getData(),
		};
	}
	getYFunction() {
		const n = random(2, 7);
		return data => Math.exp(-n * data.x) * Math.sin(1.2 * n * Math.PI * data.x);
	}

	generateRandomData(points = 6) {
		return range(1, points + 1).map(i => ({ x: i, y: i + random(-1, 2) }));
	}

	getData() {
		return range(1, 10).map(i => ({ x: i, y: random(1, 8) }));
	}

	getStyles() {
		const colors = ["red", "orange", "magenta", "gold", "blue", "purple"];
		return {
			stroke: colors[random(0, 5)],
			strokeWidth: random(1, 5),
		};
	}

	getTransitionData() {
		const n = random(4, 10);
		return range(n).map(i => {
			return {
				x: i,
				y: random(2, 10),
			};
		});
	}

	changeScroll(scrollEnabled) {
		this.setState({ scrollEnabled });
	}

	updateDemoData() {
		this.setState({
			y: this.getYFunction(),
			style: this.getStyles(),
			transitionData: this.getTransitionData(),
			randomData: this.generateRandomData(),
			data: this.getData(),
		});
	}

	componentDidMount() {
		setInterval(this.updateDemoData.bind(this), 3000);
	}
	render() {
		return (
			<View>
				<VictoryChart animate={{ duration: 2000 }}>
					<VictoryBar
						labels={() => "Hi"}
						data={this.state.transitionData}
						style={{
							data: {
								fill: "tomato",
								width: 12,
							},
						}}
						animate={{
							onExit: {
								duration: 500,
								before: () => ({
									y: 0,
									fill: "orange",
									label: "BYE",
								}),
							},
						}}
					/>
				</VictoryChart>

				<VictoryChart>
					<VictoryStack>
						<VictoryArea
							data={[
								{ x: "a", y: 2 },
								{ x: "b", y: 3 },
								{ x: "c", y: 5 },
								{ x: "d", y: 4 },
								{ x: "e", y: 7 },
							]}
						/>
						<VictoryArea
							data={[
								{ x: "a", y: 1 },
								{ x: "b", y: 4 },
								{ x: "c", y: 5 },
								{ x: "d", y: 7 },
								{ x: "e", y: 5 },
							]}
						/>
						<VictoryArea
							data={[
								{ x: "a", y: 3 },
								{ x: "b", y: 2 },
								{ x: "c", y: 6 },
								{ x: "d", y: 2 },
								{ x: "e", y: 6 },
							]}
						/>
						<VictoryArea
							data={[
								{ x: "a", y: 2 },
								{ x: "b", y: 3 },
								{ x: "c", y: 3 },
								{ x: "d", y: 4 },
								{ x: "e", y: 7 },
							]}
						/>
					</VictoryStack>
				</VictoryChart>

				<VictoryLine
					style={{ data: this.state.style }}
					interpolation="basis"
					animate={{ duration: 1500 }}
					y={this.state.y}
				/>

				<VictoryArea
					data={[
						{ amount: 1, yield: 1, error: 0.5 },
						{ amount: 2, yield: 2, error: 1.1 },
						{ amount: 3, yield: 3, error: 0 },
						{ amount: 4, yield: 2, error: 0.1 },
						{ amount: 5, yield: 1, error: 1.5 },
					]}
					x={"amount"}
					y={data => data.yield + data.error}
				/>

				<VictoryArea
					interpolation="basis"
					animate={{ duration: 1500 }}
					data={this.state.data}
				/>

				<VictoryGroup
					width={300}
					height={375}
					style={{ data: { opacity: 0.3 } }}
				>
					<VictoryArea
						data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]}
					/>
					<VictoryArea
						data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 1 }]}
					/>
					<VictoryArea
						data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 2 }]}
					/>
				</VictoryGroup>

				<VictoryStack width={300} height={375}>
					<VictoryArea
						data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]}
					/>
					<VictoryArea
						data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 1 }]}
					/>
					<VictoryArea
						data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 2 }]}
					/>
				</VictoryStack>

				<VictoryStack
					width={300}
					height={450}
					style={{
						data: {
							strokeDasharray: "5,5",
							strokeWidth: 2,
							fillOpacity: 0.4,
						},
					}}
				>
					<VictoryArea
						style={{
							data: {
								fill: "tomato",
								stroke: "tomato",
							},
						}}
						data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]}
					/>
					<VictoryArea
						style={{
							data: {
								fill: "orange",
								stroke: "orange",
							},
						}}
						data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 1 }]}
					/>
					<VictoryArea
						style={{
							data: {
								fill: "gold",
								stroke: "gold",
							},
						}}
						data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 2 }]}
					/>
				</VictoryStack>

				<Text style={styles.text}>{"<VictoryBar />"}</Text>

				<VictoryBar />

				<VictoryBar
					data={[
						{ x: 1, y: 1 },
						{ x: 2, y: 2 },
						{ x: 3, y: 3 },
						{ x: 4, y: 2 },
						{ x: 5, y: 1 },
					]}
				/>

				<VictoryGroup
					width={300}
					height={375}
					offset={20}
					colorScale={"qualitative"}
				>
					<VictoryBar data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]} />
					<VictoryBar data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 1 }]} />
					<VictoryBar data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 2 }]} />
				</VictoryGroup>

				<VictoryStack width={300} height={375} colorScale={"qualitative"}>
					<VictoryBar data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 3 }]} />
					<VictoryBar data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 1 }]} />
					<VictoryBar data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 2 }]} />
				</VictoryStack>

				<VictoryBar
					height={375}
					padding={75}
					style={{
						data: {
							fill: data => (data.y > 2 ? "red" : "blue"),
						},
					}}
					data={[
						{ x: 1, y: 1 },
						{ x: 2, y: 2 },
						{ x: 3, y: 3 },
						{ x: 4, y: 2 },
						{ x: 5, y: 1 },
					]}
				/>
			</View>
		);
	}
}
