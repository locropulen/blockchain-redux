export default function statusMiddleware(cb) {
	return function subscriber({ getState, dispatch }) {
		return function(dispatch) {
			return function logDispatch(action) {
				cb(action, getState());
				dispatch(action);
				// console.log("...after:", getState());
			};
		};
	};
}
