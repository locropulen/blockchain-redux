import rootReducer from "./rootReducer";

import {
	createStore,
	eventEmitterMiddleware,
	applyMiddleware,
	Block,
} from "reduxu-chain";

import statusMiddleware from "./statusMiddleware";

export default class ReduxuStore {
	constructor() {
		this.store = createStore(rootReducer);		
		applyMiddleware(this.store, eventEmitterMiddleware, statusMiddleware);		
	}

	addBlock(block) {
		return this.store.addBlock(block);
	}
	mineBlock(block) {
		return this.store.mineBlock(block);
	}
	getLastBlock() {
		return this.store.getLastBlock();
	}
	addTransaction() {
		const tx = {
			timestamp: new Date().getTime() / 1000,
		};
		this.store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});
	}
	mineChain(data) {
		const lastBlock = this.store.getLastBlock();
		const block = new Block({
			previousBlock: lastBlock,
			data,
		});
		return mineBlock(this.store, block).then(() => {
			return addBlock(this.store, block);
		});
	}

}
