/* @flow */

import * as React from "react";
import { ScrollView, Text, StyleSheet } from "react-native";
import {
	Title,
	Caption,
	Paragraph,
	Card,
	CardCover,
	CardContent,
	CardActions,
	Button,
	withTheme,
} from "react-native-paper";
import ReduxuStore from "./ReduxuStore";

// const store1 = new ReduxuStore();
// const store2 = new ReduxuStore();

type Props = {
	theme: *,
};

class CardExample extends React.Component<Props> {
	static title = "Card";
	state = {
		block1: {
			blockInfo: [],
			transactions: "0",
		},
		block2: {
			blockInfo: [],
			transactions: "0",
		},
	};
	store1 = new ReduxuStore();
	store2 = new ReduxuStore();
	componentDidMount() {
		// mineChain(action => {
		// 	this.setState({
		// 		action,
		// 	});
		// });
	}
	addTransaction() {
		this.store1.addTransaction();
		this.setState({
			block1: {
				blockInfo: this.store1.getLastBlock(),
				transactions: this.store1.getLastBlock().data.length.toString(),
			},
		});
	}
	render() {
		const { theme: { colors: { background } } } = this.props;
		return (
			<ScrollView
				style={[styles.container, { backgroundColor: background }]}
				contentContainerStyle={styles.content}
			>
				<Card>
					<CardContent>
						<Title>redux-blockchain</Title>
						<Caption>Key concepts</Caption>
						<Paragraph>
							This is an expo sample app showing a tiny redux-implementation of
							the blockchain including simple proof-of-work or proof-of-stake
							implementation using nonce built by rollup
						</Paragraph>
					</CardContent>
				</Card>
				<Card>
					<CardActions>
						<Button
							icon="add-a-photo"
							primary
							onPress={() => this.addTransaction()}
						>
							Add transaction
						</Button>
						<Button icon="add-a-photo" primary>
							Mine block
						</Button>
					</CardActions>
				</Card>
				<Card>
					<CardContent>
						<Title>Store 1 Local Block Info</Title>
						<Caption>
							{this.state.block1.transactions} open transactions
						</Caption>
						<Paragraph>{JSON.stringify(this.state.block1.blockInfo)}</Paragraph>
					</CardContent>
				</Card>
				<Card>
					<CardContent>
						<Title>Store 2 Local Block Info</Title>
						{this.state.block2.transactions && (
							<Caption>
								{this.state.block2.transactions} open transactions
							</Caption>
						)}
						<Paragraph>{JSON.stringify(this.state.block2.blockInfo)}</Paragraph>
					</CardContent>
				</Card>
				<Card>
					<CardCover
						source={{
							uri: "https://media.giphy.com/media/dkGhBWE3SyzXW/giphy.gif",
						}}
					/>
				</Card>
				<Card>
					<CardContent>
						<Title>Block numbers</Title>
						<Caption>100</Caption>
					</CardContent>
				</Card>
				<Card>{this.props.children}</Card>
			</ScrollView>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		padding: 4,
	},
});

export default withTheme(CardExample);
