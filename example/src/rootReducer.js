export const initialState = [];

export default function rootReducer(state = initialState, action) {
	switch (action.type) {
		case "ADD_TRANSACTION":
			return [...(state || []), action.tx];
		case "@Blockchain/NEW_BLOCK":
			return action.newBlock.data;
		case "@Blockchain/INIT":
			return [];
		default:
			return state;
	}
}
