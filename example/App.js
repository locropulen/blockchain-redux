/* @flow */

// import { random, range } from "lodash";
import React, { Component } from "react";
// import { ScrollView, StyleSheet } from "react-native";
import { Provider as PaperProvider, DefaultTheme } from "react-native-paper";
import Header from "./src/Header";
import MyApp from "./src/MyApp";

export default class App extends Component {
	render() {
		return (
			<PaperProvider theme={DefaultTheme}>
				<Header>
					<MyApp />
				</Header>
			</PaperProvider>
		);
	}
}
