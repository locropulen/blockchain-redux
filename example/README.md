# redux-blockchain-example-expo

> Yet another blockchain implementation but on expo! :)

Simple blockchain implementation flow typed checked done for educational purposes.

## Getting started

Install and run expo

```
npm i -g exp
```

```
cd example-expo
yarn
exp start
```

Get the expo app to test it