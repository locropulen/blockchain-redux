/* @flow */

export default function applyMiddleware(store: *, ...args: *) {
	const middlewares = args.map(middleware => middleware(store));
	let of = store.dispatch;
	middlewares.forEach(middleware => {
		of = middleware(of);
	});

	store.dispatch = of;
}
