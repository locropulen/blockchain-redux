/* @flow */
import BlockChain from "./BlockChain";

import type { IBlock } from "./Block";
import type { State, Action } from "./";

import type { IBlockChain } from "./BlockChain";

export type Reducer = (data: State, action: Action) => State;

export interface Store {
	getState: () => State;
	dispatch: (action: Action) => void;
	subscribe: (listener: Function) => Function;
	_blockchain: IBlockChain;
	replaceChain: (newBlock: IBlock, difficulty: number) => void;
	getLastBlock: () => IBlock;
	addBlock: (block: IBlock) => Promise<*>;
	mineBlock: (block: IBlock) => Promise<*>;
}

export default function createStore(
	reducer: Reducer,
	state: State = []
): Store {
	const store = new MyStore(reducer, state);
	// no middleware/enhancer for this action
	store.dispatch({ type: "@Blockchain/INIT" });
	return store;
}

class MyStore implements Store {
	constructor(reducer: Reducer, state: State) {
		this.state = state;
		this._listeners = [];
		this.reducer = reducer;

		this.dispatch = this.dispatch.bind(this);
		this.getState = this.getState.bind(this);
		this.subscribe = this.subscribe.bind(this);

		this._blockchain = new BlockChain(this, state);
	}

	state: State;
	reducer: Reducer;
	_blockchain: IBlockChain;
	_listeners: Array<Function>;

	dispatch: (action: Action) => void;
	dispatch(action: Action) {
		this.state = this.reducer(this.state, action);
		this._listeners.forEach(listener => listener());
	}

	getState: () => State;
	getState(): State {
		return this.state;
	}

	subscribe: (listener: Function) => Function;
	subscribe(listener: Function): Function {
		this._listeners.push(listener);

		return () => {
			this._listeners.splice(this._listeners.indexOf(listener), 1);
		};
	}

	getLastBlock: () => IBlock;
	getLastBlock(): IBlock {
		return this._blockchain.getLastBlock();
	}

	mineBlock: (block: IBlock) => Promise<*>;
	mineBlock(block: IBlock): Promise<*> {
		return this._blockchain.mineBlock(block);
	}

	addBlock: (block: IBlock) => Promise<*>;
	addBlock(block: IBlock): Promise<*> {
		return this._blockchain.addBlock(block);
	}

	replaceChain: (newBlock: IBlock, difficulty: number) => void;
	// primarily used when starting up to take latest available blockchain
	replaceChain(newBlock: IBlock, difficulty: number) {
		return this._blockchain.replaceChain(newBlock, difficulty);
	}
}
