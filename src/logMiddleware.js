/* @flow */

import type { Action, State } from "../src";

export default function logMiddleware({
	getState,
	dispatch,
}: {
	getState: () => State,
	dispatch: *,
}) {
	return function(dispatch: *) {
		return function logDispatch(action: Action) {
			console.log("...before:", getState());
			console.log("...action:", action);
			dispatch(action);
			console.log("...after:", getState());
		};
	};
}
