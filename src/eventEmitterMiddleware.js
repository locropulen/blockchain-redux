/* @flow */

import type { Action, Store } from "./";

// todo: not sure if that is important but event emitter is not shared among stores
export default function middleWare(emitter: *) {
	return function eventEmitterMiddleware(store: Store) {
		emitter.on("blocks_received", (action: Action) => {
			console.log("store.getState()");
			console.log(action);
			console.log(store.getState());
			if (action.type === "@Blockchain/NEW_BLOCK") {
				// update my local blockchain difficulty and blocks
				store.replaceChain(action.newBlock, action.difficulty);
			}
		});
		return function(dispatch: *) {
			return function eventEmitterDispatch(action: Action) {
				if (action.type === "@Blockchain/NEW_BLOCK") {
					// fire an event when accepted a new block
					emitter.emit("blocks_received", action);
				}
				dispatch(action);
			};
		};
	};
}
