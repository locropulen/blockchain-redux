/* @flow */

import CryptoJS from "crypto-js";
import type { State } from "./";

export interface IBlock {
	hash: string;
	data: State;

	nonce: number;

	// optional props
	previousHash: string;

	// private ones
	+timestamp: number;
	_data: string;
}

class Block implements IBlock {
	index: number;
	hash: string;

	previousHash: string;
	nonce: number;
	timestamp: number;
	_data: string;

	constructor({ previousBlock, data }: { previousBlock: IBlock, data: State }) {
		this.previousHash = previousBlock.hash.toString();
		this.timestamp = new Date().getTime() / 1000;
		this.data = data;
		this.nonce = 0;
		this.hash = Block.calculateHash(this);
	}

	set data(data: State) {
		this._data = JSON.stringify(data);
	}

	get data(): State {
		return JSON.parse(this._data);
	}

	// this is where a Proof-of-Work or Proof-of-Stake algo comes in
	static calculateHash(block: IBlock) {
		return CryptoJS.SHA256(
			block.previousHash + block.timestamp + block._data + block.nonce
		).toString();
	}
}

export default Block;
