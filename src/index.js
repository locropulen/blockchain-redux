// @flow

import createStore from "./createStore";
import applyMiddleware from "./applyMiddleware";
import Block from "./Block";
import logMiddleware from "./logMiddleware";
import eventEmitterMiddleware from "./eventEmitterMiddleware";

import type { IBlock } from "./Block";
export type { IBlock } from "./Block";
export type { IBlockChain } from "./BlockChain";
export type { Store } from "./createStore";

export type BlockchainAction =
	| {
			type: "@Blockchain/INIT",
		}
	| {
			type: "@Blockchain/NEW_BLOCK",
			newBlock: IBlock,
			difficulty: number,
		};

export type Action =
	| BlockchainAction
	| {
			type: "ADD_TRANSACTION",
			tx: any,
		};

export type State = Array<*>;

export {
	createStore,
	applyMiddleware,
	Block,
	eventEmitterMiddleware,
	logMiddleware,
};
