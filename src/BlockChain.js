// @flow
import type { IBlock } from "./Block";
import Block from "./Block";
import type { Store, State } from "./";

export interface IBlockChain {
	chain: Array<IBlock>;
	difficulty: number;
	addBlock: (newBlock: IBlock) => Promise<*>;
	getLastBlock: () => IBlock;
	replaceChain: (newBlock: IBlock, difficulty: number) => void;
	mineBlock: (newBlock: IBlock) => Promise<*>;
}

export default class Blockchain implements IBlockChain {
	constructor(Store: Store, state: State) {
		this._store = Store;
		this.chain = [this.createGenesisBlock(state)];
		this.difficulty = 3;
	}

	_store: Store;
	chain: Array<IBlock>;
	difficulty: number;

	createGenesisBlock(currentState: State) {
		// $FlowFixMe
		return new Block({
			previousBlock: {
				index: 0,
				hash: "0",
				timestamp: new Date().getTime(),
			},
			data: currentState,
		});
	}
	proofOfStake(newBlock: IBlock): boolean {
		return (
			newBlock.hash.substring(0, this.difficulty) ===
			Array(this.difficulty + 1).join("0")
		);
	}
	isValidNewBlock(newBlock: IBlock, previousBlock: IBlock): boolean {
		if (!this.proofOfStake(newBlock)) {
			console.warn("invalid mined block");

			return false;
		}

		if (previousBlock.hash !== newBlock.previousHash) {
			console.warn("invalid previoushash");
			return false;
		}

		return true;
	}

	isValidChain(blockchain: Array<IBlock>) {
		for (let i = 0; i < blockchain.length - 1; i++) {
			if (!this.isValidNewBlock(blockchain[i + 1], blockchain[i])) {
				return false;
			}
		}
		return true;
	}
	// discover nonce
	mineBlock: (newBlock: IBlock) => Promise<*>;
	mineBlock(newBlock: IBlock): Promise<*> {
		return new Promise((resolve, reject) => {
			while (!this.proofOfStake(newBlock)) {
				newBlock.nonce++;
				newBlock.hash = Block.calculateHash(newBlock);
				if (newBlock.nonce > 300000) {
					// too much huuuu
					reject("Time out");
				}
			}
			resolve();
		});
	}
	getLastBlock: () => IBlock;
	getLastBlock(): IBlock {
		return this.chain[this.chain.length - 1];
	}
	addBlock: (newBlock: IBlock) => Promise<*>;
	addBlock(newBlock: IBlock): Promise<*> {
		return new Promise((resolve, reject) => {
			if (this.isValidNewBlock(newBlock, this.getLastBlock())) {
				// cool adding new block to the chain
				this.chain.push(newBlock);
				console.log(this.getLastBlock());

				// this.difficulty++??????????
				// tell others here
				this._store.dispatch({
					type: "@Blockchain/NEW_BLOCK",
					newBlock: this.getLastBlock(),
					difficulty: this.difficulty,
				});
				return resolve(this.getLastBlock());
			}
			return reject("It is not a valid block");
		});
	}
	replaceChain: (newBlock: IBlock, difficulty: number) => void;
	replaceChain(newBlock: IBlock, difficulty: number): void {
		// TODO: replacing only chain but can change dificulty also!
		const newBlocks: Array<IBlock> = [...this.chain, newBlock];
		if (this.isValidChain(newBlocks) && newBlocks.length > this.chain.length) {
			this.chain = newBlocks;
			this.difficulty = difficulty;
		}
	}
}
