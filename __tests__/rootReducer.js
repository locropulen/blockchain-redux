/* @flow */

import createStore from "../src/createStore";

import { logMiddleware, applyMiddleware } from "../src";

import type { Action, State } from "../src";

export const initialState = [];

export default function rootReducer(
	state?: State = initialState,
	action: Action
): State {
	switch (action.type) {
		case "ADD_TRANSACTION":
			return [...(state || []), action.tx];
		case "@Blockchain/NEW_BLOCK":
			return action.newBlock.data;
		case "@Blockchain/INIT":
			return [];
		default:
			return state;
	}
}

describe("rootReducer", () => {
	it("subscribe to changes", () => {
		const store = createStore(rootReducer);

		let called = false;
		const tx = {};
		store.subscribe(() => (called = true));
		store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});
		expect(called).toBe(true);
	});
	it("apply middleware", () => {
		const store = createStore(rootReducer);
		applyMiddleware(store, logMiddleware);

		let called = false;

		store.subscribe(() => (called = true));
		const tx = {};
		store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});
		expect(called).toBe(true);
	});
	it("should dispatch @Blockchain/INIT", () => {
		const store = createStore(rootReducer);
		expect(store.getState().length).toBe(0);
	});

	it("unsubscribe from changes", () => {
		const store = createStore(rootReducer);

		let called = 0;

		const unsub = store.subscribe(() => (called += 1));
		const tx = {};

		store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});
		unsub();

		store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});

		expect(called).toBe(1);
	});
});
