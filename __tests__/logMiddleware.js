/* @flow */

import createStore from "../src/createStore";
import rootReducer from "./rootReducer";
import { applyMiddleware } from "../src";

import logMiddleware from "../src/logMiddleware";

describe("apply middleware", () => {
	it("apply middleware", () => {
		const store = createStore(rootReducer);
		applyMiddleware(store, logMiddleware);

		let called = false;

		store.subscribe(() => (called = true));

		const tx = {};
		store.dispatch({
			type: "ADD_TRANSACTION",
			tx,
		});
		expect(called).toBe(true);
	});
});
