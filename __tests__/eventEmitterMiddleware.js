/* @flow */
import createStore from "../src/createStore";
import rootReducer from "./rootReducer";
import mitt from "mitt";
import { applyMiddleware, Block } from "../src";

import eventEmitterMiddleware from "../src/eventEmitterMiddleware";

import type { IBlockChain } from "../src";

describe("apply middleware", () => {
	it("apply event emitter middleware", done => {
		let emitter = mitt();
		let shouldDoneCount = 0;
		const store1 = createStore(rootReducer);
		applyMiddleware(store1, eventEmitterMiddleware(emitter));

		const store2 = createStore(rootReducer);
		applyMiddleware(store2, eventEmitterMiddleware(emitter));

		const lastBlock1 = store1.getLastBlock();
		const block1 = new Block({
			previousBlock: lastBlock1,
			data: [{}],
		});
		const handleCallback = done => {
			if (shouldDoneCount == 2) {
				done();
			}
			shouldDoneCount++;
		};
		expect(store1.getState().length).toBe(0);
		expect(store2.getState().length).toBe(0);

		store1.mineBlock(block1).then(() => {
			store1.addBlock(block1).then(
				(_: IBlockChain) => {
					expect(store1.getState().length).toBe(1);
					expect(store2.getState().length).toBe(1);
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
		const lastBlock2 = store2.getLastBlock();
		const block2 = new Block({
			previousBlock: lastBlock2,
			data: [],
		});
		store2.mineBlock(block2).then(() => {
			store2.addBlock(block2).then(
				(_: IBlockChain) => {
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
		const store3 = createStore(rootReducer);
		const lastBlock3 = store3.getLastBlock();
		const block3 = new Block({
			previousBlock: lastBlock3,
			data: [],
		});

		store3.mineBlock(block3).then(() => {
			store3.addBlock(block3).then(
				(_: IBlockChain) => {
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
	});
});
