/* @flow */

import Block from "../src/Block";
import createStore from "../src/createStore";
import rootReducer from "./rootReducer";

import { logMiddleware, eventEmitterMiddleware, applyMiddleware } from "../src";
import mitt from "mitt";

import type { IBlockChain } from "../src/BlockChain";

describe("Minning", () => {
	it("it shouldn't replace the blockhain because it is not mined!", () => {
		const store1 = createStore(rootReducer);
		const store2 = createStore(rootReducer);

		for (let i = 0; i < 5; i++) {
			const tx = {};
			store1.dispatch({
				type: "ADD_TRANSACTION",
				tx,
			});
		}

		for (let i = 0; i < 10; i++) {
			const tx = {};
			store2.dispatch({
				type: "ADD_TRANSACTION",
				tx,
			});
		}

		store1.replaceChain(store2.getLastBlock(), 3);
		expect(store1.getState().length).toBe(5);
	});
	// todo: implement test case
	it("update the block in the local blockchain and updates others stores by eventEmitterMiddleware", done => {
		const store = createStore(rootReducer);
		let emitter = mitt();

		applyMiddleware(store, eventEmitterMiddleware(emitter));

		const store2 = createStore(rootReducer);
		applyMiddleware(store2, eventEmitterMiddleware(emitter));

		const lastBlock = store.getLastBlock();

		// this one should be accepted
		const block = new Block({
			previousBlock: lastBlock,
			data: [{}, {}, {}],
		});
		store.mineBlock(block).then(() => {
			console.log("store2");
			console.log(store2);
			// after minned block has the right nonce
			store.addBlock(block).then(() => {
				expect(store2.getState().length).toBe(3);
				done();
			});
			// store.replaceChain is not needed since store is already updated!
			// difficulty 1
		});
	});

	it("update the block in the local blockchain", done => {
		const store = createStore(rootReducer);

		const lastBlock = store.getLastBlock();

		// this one should be accepted
		const block = new Block({
			previousBlock: lastBlock,
			data: [{}, {}, {}],
		});
		store.mineBlock(block).then(() => {
			// after minned block has the right nonce
			store.addBlock(block).then(() => {
				expect(block.data.length).toBe(3);
				expect(store.getState().length).toBe(3);
				done();
			});
		});
	});

	it("reject bad blocks when previous hash is invalid", done => {
		const store = createStore(rootReducer);

		// this one should be rejected, bad previous hash
		const block1 = new Block({
			previousBlock: store.getLastBlock(),
			data: [{}, {}],
		});
		block1.previousHash = "random";
		store.addBlock(block1).then(
			() => {},
			_ => {
				expect(store.getState().length).toBe(0);
				done();
			}
		);
	});

	it("reject bad blocks when hash is invalid", done => {
		const store = createStore(rootReducer);

		// this one should be rejected, bad previous hash
		const block1 = new Block({
			previousBlock: store.getLastBlock(),
			data: [{}, {}],
		});
		block1.hash = "random";
		store.addBlock(block1).then(
			() => {},
			_ => {
				expect(store.getState().length).toBe(0);
				done();
			}
		);
	});

	it("add three blocks apply log middleware", done => {
		let shouldDoneCount = 0;
		const store1 = createStore(rootReducer);
		let emitter = mitt();

		applyMiddleware(store1, logMiddleware, eventEmitterMiddleware(emitter));

		const lastBlock1 = store1.getLastBlock();
		const block1 = new Block({
			previousBlock: lastBlock1,
			data: [{}, {}],
		});
		const handleCallback = done => {
			if (shouldDoneCount == 2) {
				done();
			}
			shouldDoneCount++;
		};

		store1.mineBlock(block1).then(() => {
			store1.addBlock(block1).then(
				(_: IBlockChain) => {
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
		const store2 = createStore(rootReducer);

		applyMiddleware(store2, logMiddleware, eventEmitterMiddleware(emitter));

		const lastBlock2 = store1.getLastBlock();
		const block2 = new Block({
			previousBlock: lastBlock2,
			data: [{}, {}],
		});
		store2.mineBlock(block2).then(() => {
			store2.addBlock(block2).then(
				(_: IBlockChain) => {
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
		const store3 = createStore(rootReducer);
		const lastBlock3 = store1.getLastBlock();
		const block3 = new Block({
			previousBlock: lastBlock3,
			data: [{}, {}],
		});

		store3.mineBlock(block3).then(() => {
			store3.addBlock(block2).then(
				(_: IBlockChain) => {
					handleCallback(done);
				},
				err => {
					console.warn(err);
					handleCallback(done);
				}
			);
		});
	});
});
